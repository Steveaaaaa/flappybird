﻿#include "mainscene.h"
#include<config.h>
#include<QIcon>
#include<QTimer>
#include<QPainter>
#include<QKeyEvent>
#include<QTime>
#include<QVector>
#include<tube.h>
#include<ctime>
#include<bird.h>
#include<iostream>
#include<QCoreApplication>
#include<QEventLoop>
#include<QMainWindow>
#include<QPixmap>
using namespace std;
mainScene::mainScene(QWidget *parent)
    : QWidget(parent)
{
    setFixedSize(GAME_WIDTH,GAME_HEIGHT);
    setWindowTitle(GAME_TITLE);
    setWindowIcon((QIcon)GAME_ICON);

    bird_Life = 1;
    life_x = 0;
    coin_number = 0;
    apple_number = 0;
    bird_number = 4;

    sound = new QSound(":/res/Sean & Bobo - Flappy Bird (Original Mix).wav",this);
    sound->play();
    sound->setLoops(-1);
    rate_show = new QLabel(this);
    m_Timer = new QTimer(this);
//读取文件中的存档信息，以购买的生命值，金币存款，排名前五的历史记录（历史记录没来得及在游戏内呈现）
    readData_coins();
    readData_scores();
    readApple();
    readBird();
    if(apple_number >= 2){
        bird_Life = 3;
        apple_number -= 2;
        bird.m_Life = bird_Life;
    }else if(apple_number == 1){
        bird_Life = 2;
        apple_number = 0;
        bird.m_Life = bird_Life;
    }else{
        bird.m_Life = bird_Life;
    }
    bird_number = 4;
//按钮指针实例化
    suspend_button = new QPushButton("",this);
    play_button = new QPushButton("",this);
    restart_button = new QPushButton("",this);
    back_button = new QPushButton("",this);

//图片载入
    ready_image.load(":/res/ready_cartoon.png");
    over_image.load(":/res/game_over.png");
    suspend_image.load(":/res/suspend_button.png");
    play_image.load(":/res/play_button.png");
    restart_image.load(":/res/restart_button.png");
    back_image.load(":/res/back_button.png");
    rate_image.load(":/res/rate.png");
    life_image.load(":/res/red_apple.png");

//调整部分图片大小
    over_image = over_image.scaled(over_image.width()*3,over_image.height()*3,Qt::KeepAspectRatio);
    suspend_image = suspend_image.scaled(suspend_image.width()*2,suspend_image.height()*2,Qt::KeepAspectRatio);
    restart_image = restart_image.scaled(restart_image.width()*2,restart_image.height()*2,Qt::KeepAspectRatio);
    play_image = play_image.scaled(play_image.width()*2,play_image.height()*2,Qt::KeepAspectRatio);
    back_image = back_image.scaled(back_image.width()*2,back_image.height()*2,Qt::KeepAspectRatio);
    life_image = life_image.scaled(life_image.width()*1.5,life_image.height()*1.5,Qt::KeepAspectRatio);


    rate_show->move(82,20);
    rate_show->resize(24,36);
//为按钮加上图片并调整大小
    suspend_button->resize(suspend_image.width(),suspend_image.height());
    suspend_button->setIcon(QIcon(suspend_image));
    suspend_button->setIconSize(QSize(suspend_image.width(),suspend_image.height()));
    suspend_button->setFlat(true);
    suspend_button->setStyleSheet("border:0px");
    suspend_button->move(900-52,0);


    play_button->resize(play_image.width(),play_image.height());
    play_button->setIcon(QIcon(play_image));
    play_button->setIconSize(QSize(play_image.width(),play_image.height()));
    play_button->setFlat(true);
    play_button->setStyleSheet("border:0px");
    play_button->move(450-play_image.width()*0.5,150);



    restart_button->resize(restart_image.width(),restart_image.height());
    restart_button->setIcon(QIcon(restart_image));
    restart_button->setIconSize(QSize(restart_image.width(),restart_image.height()));
    restart_button->setFlat(true);
    restart_button->move(450-restart_image.width()*0.5,250);


    back_button->resize(back_image.width(),back_image.height());
    back_button->setIcon(QIcon(back_image));
    back_button->setIconSize(QSize(back_image.width(),back_image.height()));
    back_button->setFlat(true);
    back_button->move(450-back_image.width()*0.5,350);


//窗口对象实例化

    s = new startScene(this);
    shop = new Shop(this);
    style = new Style(this);
    style->hide();
    shop->hide();
//信号槽：子窗口的start被按下时游戏开始
    connect(s,&startScene::startSignal,[=](){
        readData_coins();
        readApple();
        cout<<apple_number<<endl;
        if(apple_number >= 2){
            bird_Life = 3;
            apple_number -= 2;
            bird.m_Life = bird_Life;
        }else if(apple_number == 1){
            bird_Life = 2;
            apple_number = 0;
            bird.m_Life = bird_Life;
        }else{
            bird.m_Life = bird_Life;
        }
        initScene();
        toMainScene();
        playGame();

    });

    connect(suspend_button,&QPushButton::clicked,this,&mainScene::suspendGame);
    connect(play_button,&QPushButton::clicked,this,&mainScene::continueGame);
    connect(restart_button,&QPushButton::clicked,this,&mainScene::restartGame);
    connect(back_button,&QPushButton::clicked,this,&mainScene::backToStartScene);
    connect(s,&startScene::shopSignal,[=](){
        toShopScene();
        shop->readApple();
        shop->readBird();
        shop->writeData_coin();
        shop->m_Timer->start();
        shop->label->setNum(coin_number);

    });
    connect(s,&startScene::styleSignal,[=](){
        toStyleScene();
        style->m_Timer->start();
    });

    connect(shop,&Shop::backToSignal,this,&mainScene::shopToStartScene);
    connect(style,&Style::backToSignal,this,&mainScene::styleToStartScene);
}

mainScene::~mainScene()
{

}
//初始化小鸟的坐标以及速度等参数

float beginV = 0;
float& pV = beginV;
int y_up = GAME_HEIGHT;
int y_down = 0;
int& pY_up = y_up;
int& pY_down = y_down;
int bird_min = 0;
int bird_max = 2;
int& m_Min = bird_min;
int& m_Max = bird_max;
int tube_interval = TUBE_INTERVAL;
int rate = 2;

//场景载入函数实现
void mainScene::initScene(){
    delete m_Timer;
    m_Timer = new QTimer;
    m_Timer->setInterval(GAME_RATE);

    m_recorder = 0;
    beginV = 0;
    bird.setPosition(100,GAME_HEIGHT*0.5);
    borad.score = 0;
    map1.m_scroll_speed = MAP_SCROLL_SPEED;
    rate = 2;

    for(int i = 0;i < TUBE_NUM;i++){
        tubes_up[i].m_X = -100;
        tubes_up[i].m_Y = 0;
        tubes_down[i].m_X = -100;
        tubes_down[i].m_Y = 0;
        tubes_up[i].m_Scoreed = true;
        tubes_up[i].m_Speed = TUBE_SPEED;
        tubes_down[i].m_Speed = TUBE_SPEED;
    }
    for(int j = 0;j < COIN_NUM;j++){
        coins[j].m_X = -100;
        coins[j].m_Y = 0;
        coins[j].m_Free = true;
        coins[j].m_Scoreed = true;
        coins[j].coin_image.load(":/res/coin.png");
        coins[j].m_Speed = TUBE_SPEED;
    }

    suspend_button->setVisible(true);
    suspend_button->setEnabled(true);
    play_button->setVisible(false);
    play_button->setEnabled(false);
    restart_button->setVisible(false);
    restart_button->setEnabled(false);
    back_button->setVisible(false);
    back_button->setEnabled(false);
}
//游戏开始函数，计时器实时更新游戏
void mainScene::playGame(){

    m_Timer->start();

    updatePosition();
    bird.setPosition(bird.m_X,calculateTrack(bird.m_Y,pV));//小鸟轨迹计算结果的显示

    tubeToScene();
    checkOver();
    //delayMSec(1000);
    connect(m_Timer,&QTimer::timeout,[=](){
        updatePosition();
        bird.setPosition(bird.m_X,calculateTrack(bird.m_Y,pV));//小鸟轨迹计算结果的显示
        scoreCounting();
        borad.getHundred(borad.score);
        borad.getTen(borad.score);
        borad.getUnit(borad.score);
        tubeToScene();
        checkCoin();
        checkScore();
        checkOver();
        rateShow();
        update();
    });
}
//坐标计算函数：背景地图坐标计算，管道数组坐标计算，小鸟贴图更新
void mainScene::updatePosition(){
    map1.mapPosition();

    for(int i = 0;i<TUBE_NUM;i++){
        if(tubes_up[i].m_Free == false){

            tubes_up[i].tubePosition();
            tubes_down[i].tubePosition();

        }
    }
    for(int j = 0;j < COIN_NUM;j++){
        if(coins[j].m_Free == false){
            coins[j].coinPosition();
        }
    }
    bird.updateInfo(m_Min,m_Max);

    if(bird.m_Y>510){
        m_Timer->stop();
    }
}
//绘图方法
void mainScene::paintEvent(QPaintEvent *event){

    QPainter painter(this);

    painter.drawPixmap(map1.m_map1_posX,0,map1.m_map1);
    painter.drawPixmap(map1.m_map2_posX,0,map1.m_map2);
    painter.drawPixmap(bird.m_X,bird.m_Y,bird.m_birdArr[bird.m_index]);

   // QTimer::singleShot()

    for(int i = 0;i < TUBE_NUM;i++){
       if(tubes_up[i].m_Free==false){
            painter.drawPixmap(tubes_up[i].m_X,tubes_up[i].m_Y,tubes_up[i].m_tube_up);
            painter.drawPixmap(tubes_down[i].m_X,tubes_down[i].m_Y,tubes_down[i].m_tube_down);
       }
    }
    for(int j = 0;j < COIN_NUM;j++){
        if(coins[j].m_Free == false){
            painter.drawPixmap(coins[j].m_X,coins[j].m_Y,coins[j].coin_image);
        }
    }
    painter.drawPixmap(450-36,20,borad.hundred[borad.getHundred(borad.score)]);
    painter.drawPixmap(450-12,20,borad.ten[borad.getTen(borad.score)]);
    painter.drawPixmap(450+12,20,borad.unit[borad.getUnit(borad.score)]);
    painter.drawPixmap(10,20,rate_image);
    for(int i = 0;i < bird.m_Life;i++){
        painter.drawPixmap(life_x+50*i,445,life_image);

    }
    if(checkOver() ==true){
        painter.drawPixmap(450-over_image.width()*0.5,100,over_image);
//    }else{

    }
}
//键盘事件监听
void mainScene::keyPressEvent(QKeyEvent *e){

//小鸟起跳
    if(e->key() == Qt::Key_W){
        pV = INITIAL_SPEED;
//令游戏暂停或者继续
    }else if(e->key() == Qt::Key_S){
        if(m_Timer->isActive()){
            suspendGame();
        }else{
            continueGame();
        }
//游戏内更换小鸟样式
    }else if(e->key() == Qt::Key_C){

            m_Min = m_Min + 3;
            m_Max = m_Max + 3;
            cout<<bird_number<<endl;
            if(m_Max > bird_number*3-1){
            m_Min = 0;
            m_Max = 2;
        }
    }else if(e->key() == Qt::Key_A){
        bird_Life--;
    }else if(e->key() == Qt::Key_D){
        bird_Life++;
    }
}
//小鸟轨迹计算函数(模拟重力加速度计算，返回每个时间间隔内y的差值)
float mainScene::calculateTrack(float y,float& pV){
    float Y = y;
    Y -= pV;
    pV -= GRAVITY;
    return Y;
}
//取得每一对管道的出现时，出事的上下两个y值
void mainScene::getTube(int &y_up, int &y_down){

    int m =qrand()%(170);

    int min = 320-m+SMALLEST_INTERVAL;
    int n = qrand()%(50);
    y_down = -m-60;
    y_up = n+min;
}
//随机管道出现的实现
void mainScene::tubeToScene(){
    m_recorder++;
    if(m_recorder < tube_interval){
        return;
    }
    int n = qrand()%10000;
    m_recorder = 0;
    if((n%3==0)||(n%7==0)||(n%5==0)){
        if((n<1000)||(n>4500)){
        for(int i = 0;i < TUBE_NUM;i++){
            if(tubes_up[i].m_Free){
                tubes_up[i].m_Free = false;
                tubes_down[i].m_Free = false;
                tubes_up[i].m_X = GAME_WIDTH;
                tubes_down[i].m_X = GAME_WIDTH;
                getTube(pY_up,pY_down);
                tubes_up[i].m_Y = y_up;
                tubes_down[i].m_Y = y_down;
                tubes_up[i].m_Scoreed = false;
                break;
                }
            }
        }else{
            int now_Y = qrand()%(200)+200;
            int now_X = 900;
            for(int i = 0;i < 3;i++){
                for(int j = 0;j<COIN_NUM;j++){
                    if(coins[j].m_Free){
                        coins[j].m_Free = false;
                        coins[j].m_X = now_X;
                        coins[j].m_Y = now_Y;
                        now_X += 42;
                        coins[j].m_Scoreed = false;
                        coins[j].coin_image.load(":/res/coin.png");
                        coins[j].coin_image = coins[j].coin_image.scaled(coins[j].coin_image.width()*0.5,coins[j].coin_image.height()*0.5,Qt::KeepAspectRatio);
                        break;
                    }
                }
            }
        }
    }
}
void mainScene::scoreCounting(){
    for(int i = 0;i < TUBE_NUM;i++){
        if((tubes_up[i].m_Scoreed==false)&&(tubes_up[i].m_Free == false)){
            if((tubes_up[i].m_X+52)<=bird.m_X){
                borad.score++;
                tubes_up[i].m_Scoreed = true;

            }
        }
    }
}

//延时计时器方法
void mainScene::delayMSec(unsigned int msec)
{
    QTime Time_set = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < Time_set )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
//子窗口跳到游戏界面的函数
void mainScene::toMainScene(){
    s->hide();
    this->show();
}
//判断游戏是否结束的函数
bool mainScene::checkOver(){
    for(int i = 0;i < TUBE_NUM;i++){
        if((tubes_up[i].m_Rect.intersects(bird.m_Rect))||(tubes_down[i].m_Rect.intersects(bird.m_Rect))||(bird.m_Y > 510)){
            bird.m_Life -=1;
            if(bird.m_Life < 1){
                m_Timer->stop();
                if(borad.score > scores_ranking[4]){
                    for(int i = 0;i < 5;i++){
                        if(borad.score>=scores_ranking[i]){
                            scores_ranking[i] = borad.score;
                            break;
                        }
                    }
                }
                //cout<<getCoins<<endl;
                coin_number += getCoins;
                getCoins = 0;
                bird_Life = 1;
                writeData_scores();
                writeData_coins();
                writeApple();
                gameOver();               
                return true;
            }
            else{

                delete m_Timer;
                m_Timer = new QTimer;
                delayMSec(2000);
                int storage = borad.score;
                initScene();
                playGame();
                borad.score = storage;
                return false;
            }

        }
    }
    return false;
}
void mainScene::checkCoin(){
    for(int j = 0;j < COIN_NUM;j++){
        if((coins[j].m_Rect.intersects(bird.m_Rect))&&(coins[j].m_Scoreed==false)){
            coins[j].coin_image.load("");
            borad.score += 3;
            getCoins++;
            coins[j].m_Scoreed = true;
        }
    }
}

void mainScene::checkScore(){
    if((borad.score>200)&&(borad.score < 400)){
        map1.m_scroll_speed = 3;
        for(int i = 0;i < TUBE_NUM;i++){
            tubes_up[i].m_Speed = 3;
            tubes_down[i].m_Speed = 3;
            coins[i].m_Speed = 3;
            rate = 3;
        }
    }else if(borad.score > 400){
        map1.m_scroll_speed = 4;
        for(int i = 0;i < TUBE_NUM;i++){
            tubes_up[i].m_Speed = 4;
            tubes_down[i].m_Speed = 4;
            coins[i].m_Speed = 4;
            rate = 4;
        }
    }
}
void mainScene::rateShow(){
    rate_show->setPixmap(borad.unit[rate-1]);
}

//游戏暂停时设置按钮是否出现是否可用的函数
void mainScene::suspendGame(){
    m_Timer->stop();
    suspend_button->setVisible(false);
    suspend_button->setEnabled(false);

    play_button->setVisible(true);
    play_button->setEnabled(true);

    restart_button->setVisible(true);
    restart_button->setEnabled(true);

    back_button->setVisible(true);
    back_button->setEnabled(true);
}
//游戏继续时设置按钮是否出现是否可用的函数
void mainScene::continueGame(){

    m_Timer->start();
    suspend_button->setVisible(true);
    suspend_button->setEnabled(true);

    play_button->setVisible(false);
    play_button->setEnabled(false);

    restart_button->setVisible(false);
    restart_button->setEnabled(false);

    back_button->setVisible(false);
    back_button->setEnabled(false);
}
//游戏结束时设置按钮是否出现是否可用的函数
void mainScene::gameOver(){

    suspend_button->setVisible(false);
    suspend_button->setEnabled(false);

    play_button->setVisible(false);
    play_button->setEnabled(false);

    restart_button->setVisible(true);
    restart_button->setEnabled(true);

    back_button->setVisible(true);
    back_button->setEnabled(true);


}
void mainScene::restartGame(){
    delete m_Timer;
    m_Timer = new QTimer;

    suspend_button->setVisible(true);
    suspend_button->setEnabled(true);

    play_button->setVisible(false);
    play_button->setEnabled(false);

    restart_button->setVisible(false);
    restart_button->setEnabled(false);

    back_button->setVisible(false);
    back_button->setEnabled(false);



    initScene();
    playGame();

    bird.m_Life = bird_Life-1;
}

void mainScene::backToStartScene(){

    delete m_Timer;
    m_Timer = new QTimer;
    m_Timer->stop();
    this->s->show();

}
void mainScene::toShopScene(){
    s->hide();
    shop->show();
}
void mainScene::shopToStartScene(){
    shop->m_Timer->stop();
    s->show();
    shop->hide();
}
void mainScene::toStyleScene(){
    s->hide();
    style->show();
}
void mainScene::styleToStartScene(){
    style->m_Timer->stop();
    s->show();
    style->hide();
}
void mainScene::writeData_coins(){
    QFile file1("coin_recoder.txt");

    if(!file1.open(QIODevice::WriteOnly)){
        cout<<"文件打开失败"<<endl;
    }else{
        QDataStream stream(&file1);
        stream<<coin_number;
        file1.close();
    }
}
void mainScene::readData_coins(){
    QFile file1("coin_recoder.txt");

    if(!file1.open(QIODevice::ReadOnly)){
        cout<<"文件打开失败"<<endl;
    }else{
        QDataStream stream(&file1);

        stream>>coin_number;

    }
}
void mainScene::writeData_scores(){
    QFile file("scores_recoder.txt");

    if(!file.open(QIODevice::WriteOnly)){
        cout<<"文件打开失败"<<endl;
    }else{
        QDataStream stream(&file);
        stream<<scores_ranking[0]<<scores_ranking[1]<<scores_ranking[2]<<scores_ranking[3]<<scores_ranking[4];

        file.close();
    }
}
void mainScene::readData_scores(){
    QFile file("scores_recoder.txt");

    if(!file.open(QIODevice::ReadOnly)){
        cout<<"文件打开失败"<<endl;
    }else{
        QDataStream stream(&file);

        stream>>scores_ranking[0]>>scores_ranking[1]>>scores_ranking[2]>>scores_ranking[3]>>scores_ranking[4];
    }
}
void mainScene::readApple(){

    QFile file("apple_recoder.txt");
    if(!file.open(QIODevice::ReadOnly)){

    }else{
        QDataStream stream(&file);
        stream>>apple_number;
    }
}
void mainScene::writeApple(){
    QFile file("apple_recoder.txt");
    if(!file.open(QIODevice::WriteOnly)){

    }else{
        QDataStream stream(&file);
        stream<<apple_number;
        file.close();
    }
}
void mainScene::readBird(){
    QFile file("bird_recoder.txt");
    if(!file.open(QIODevice::ReadOnly)){

    }else{
        QDataStream stream(&file);
        stream>>bird_number;
    }
}

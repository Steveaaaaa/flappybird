﻿#ifndef MAINSCENE_H
#define MAINSCENE_H
#include<map1.h>
#include<bird.h>
#include <QWidget>
#include<QTimer>
#include<tube.h>
#include<config.h>
#include<QVector>
#include<QSplashScreen>
#include<startscene.h>
#include<QPixmap>
#include<QPushButton>
#include<scoreborad.h>
#include<coin.h>
#include<QLabel>
#include<shop.h>
#include<QFile>
#include<QDataStream>
#include<style.h>
#include<QSound>
class mainScene : public QWidget
{
    Q_OBJECT

public:
    mainScene(QWidget *parent = 0);
    ~mainScene();
public:
    startScene *s;
    Shop *shop;
    Style *style;
    QTimer *m_Timer;
    Map1 map1;
    Bird bird;
    scoreBorad borad;
    QSound *sound;

    QPixmap ready_image;
    QPixmap over_image;
    QPixmap suspend_image;
    QPixmap play_image;
    QPixmap restart_image;
    QPixmap back_image;
    QPixmap rate_image;
    QPixmap life_image;
    QLabel *rate_show;



    QPushButton *suspend_button;
    QPushButton *play_button;
    QPushButton *restart_button;
    QPushButton *back_button;


    int tube_x = 900;
    int life_x;
    int bird_Life;
    int coin_number;
    int apple_number;
    int bird_number;
    int getCoins;

    int scores_ranking[5];
    Tube tubes_up[TUBE_NUM];
    Tube tubes_down[TUBE_NUM];
    Coin coins[COIN_NUM];

    int m_recorder;



    //游戏载入函数
    void initScene();//场景载入
    void playGame();
    void updatePosition();
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    float calculateTrack(float y,float& pV);
    void getTube(int& y_up,int& y_down);
    void tubeToScene();
    //void collisionDetection();
    void delayMSec(unsigned int msec);
    bool checkOver();
    void gameOver();
    void restartGame();
    void suspendGame();
    void continueGame();
    void scoreCounting();
    void checkCoin();
    void checkScore();
    void rateShow();
    void readData_coins();
    void readData_scores();
    void writeData_coins();
    void writeData_scores();
    void readApple();
    void writeApple();
    void readBird();
public slots:
    void toMainScene();
    void shopToStartScene();
    void backToStartScene();
    void toShopScene();
    void toStyleScene();
    void styleToStartScene();
//    void clickCartoon();
};

#endif // MAINSCENE_H

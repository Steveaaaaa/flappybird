﻿#ifndef COIN_H
#define COIN_H
#include<QPixmap>
#include<QRect>
class Coin
{
public:
    Coin();

    QPixmap coin_image;
    QRect coin_rect;

    int m_X;
    int m_Y;

    void coinPosition();

    int m_Speed;
    QRect m_Rect;
    bool m_Free;
    bool m_Scoreed;
};

#endif // COIN_H

﻿#include "bird.h"
#include<config.h>
#include<QString>
Bird::Bird()
{

    for(int i = 1 ;i <= 12 ;i++)
        {
            //字符串拼接，类似  ":/res/bomb-1.png"
            QString str = QString(BIRD_PATH).arg(i);
            m_birdArr.push_back(QPixmap(str));
        }
    m_Bird.load(BIRD_PATH1);
    m_X = 100;
    m_Y = GAME_HEIGHT*0.5;

    m_index = 0;
    m_recoder = 0;

    m_Rect.setWidth(m_Bird.width());
    m_Rect.setHeight(m_Bird.height());
    m_Rect.moveTo(m_X,m_Y);
}
void Bird::setPosition(int x,int y){
    if(y>=0){
        m_X = x;
        m_Y = y;
        m_Rect.moveTo(m_X,m_Y);
    }
}
void Bird::updateInfo(int& min,int& max){
    m_recoder++;
    if(m_recoder<BIRD_INTERVAL){
        return;
    }
    m_recoder = min;
    m_index++;

    if(m_index > max){
        m_index = min;
    }
}

﻿#include "map1.h"
#include<config.h>
Map1::Map1()
{
    m_map1.load(MAP1_PATH);
    m_map2.load(MAP1_PATH);

    m_map1_posX = GAME_WIDTH;
    m_map2_posX = 0;

    m_scroll_speed = MAP_SCROLL_SPEED;
}
//背景地图的坐标计算函数
void Map1::mapPosition(){
    m_map1_posX -= m_scroll_speed;
    if(m_map1_posX<=0){
        m_map1_posX = GAME_WIDTH;
    }
    m_map2_posX -= m_scroll_speed;
    if(m_map2_posX <= -GAME_WIDTH){
        m_map2_posX = 0;
    }
}

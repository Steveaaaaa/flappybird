﻿#ifndef MAP1_H
#define MAP1_H
#include<QPixmap>


class Map1
{
public:
    Map1();
    void mapPosition();
public:
    QPixmap m_map1;
    QPixmap m_map2;

    int m_map1_posX;
    int m_map2_posX;

    int m_scroll_speed;
};


#endif // MAP1_H

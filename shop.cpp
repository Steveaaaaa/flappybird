﻿#include "shop.h"
#include<config.h>
#include<QPainter>
#include<iostream>
#include<QMessageBox>
using namespace std;
Shop::Shop(QWidget *parent) : QWidget(parent)
{
    this->setFixedSize(GAME_WIDTH,GAME_HEIGHT);
    this->setWindowTitle(GAME_TITLE);
    this->setWindowIcon((QIcon)GAME_ICON);

    m_Timer = new QTimer;
    bird_level = 1;
    m_index = 0;
    m_recoder = 0;
    m_Timer->setInterval(GAME_RATE);


    money = 0;
    apples = 1;


    label = new QLabel(this);
//存档信息读取

    readData_coin();
    readApple();
    readBird();

    label->move(20,450);

    label->setFont(QFont("楷体",16));
    label->setStyleSheet("QLabel{background:transparent;color:white;}");
    label->resize(40,40);
    shop_title = new QLabel(this);

    copy.load(":/res/bird_1.png");

    copy = copy.scaled(copy.width()*3,copy.height()*3,Qt::KeepAspectRatio);
    title_image.load(":/res/shop_button.png");
    for(int i = 4 ;i <= 6 ;i++){
        //字符串拼接，类似  ":/res/bomb-1.png"
        QString str = QString(BIRD_PATH).arg(i);

        blue_image.push_back(QPixmap(str) = QPixmap(str).scaled(copy.width(),copy.height(),Qt::KeepAspectRatio));
    }
    for(int i = 7;i <= 9;i++){
        QString str1 = QString(BIRD_PATH).arg(i);
        red_image.push_back(QPixmap(str1) = QPixmap(str1).scaled(copy.width(),copy.height(),Qt::KeepAspectRatio));
    }
    for(int i = 10;i <= 12;i++){
        QString str2 = QString(BIRD_PATH).arg(i);
        cool_image.push_back(QPixmap(str2) = QPixmap(str2).scaled(copy.width(),copy.height(),Qt::KeepAspectRatio));

    }
    red_apple_image.load(":/res/red_apple.png");
    shop_background.load(":/res/shop_background.jpeg");
    back_image.load(":/res/back_button.png");

    title_image = title_image.scaled(title_image.width()*5,title_image.height()*5,Qt::KeepAspectRatio);
    red_apple_image = red_apple_image.scaled(red_apple_image.width()*4,red_apple_image.height()*4,Qt::KeepAspectRatio);
    back_image = back_image.scaled(back_image.width()*2,back_image.height()*2,Qt::KeepAspectRatio);

    red_apple_button = new QPushButton("",this);
    blue_bird = new QPushButton("",this);
    red_bird = new QPushButton("",this);
    cool_bird = new QPushButton("",this);
    back_button = new QPushButton("",this);

    red_apple_button->resize(red_apple_image.width(),red_apple_image.height());
    blue_bird->resize(copy.width(),copy.height());
    red_bird->resize(copy.width(),copy.height());
    cool_bird->resize(copy.width(),copy.height());


    red_apple_button->move(450-red_apple_image.width()*0.5,350-red_apple_image.height()-30);
    blue_bird->move(250-copy.width()*0.5,400-copy.height()*0.5);
    red_bird->move(400-copy.width()*0.5,400-copy.height()*0.5);
    cool_bird->move(650-copy.width()*0.5,400-copy.height()*0.5);
    back_button->move(GAME_WIDTH-back_image.width()-50,GAME_HEIGHT-back_image.height()-30);

//    red_apple_button->setVisible(false);
//    blue_bird->setVisible(false);
//    red_bird->setVisible(false);
//    cool_bird->setVisible(false);
//    red_apple_button->setEnabled(true);
//    blue_bird->setEnabled(true);
//    cool_bird->setEnabled(true);
//    red_apple_button->setEnabled(true);
    back_button->resize(back_image.width(),back_image.height());
    back_button->setIcon(QIcon(back_image));
    back_button->setIconSize(QSize(back_image.width(),back_image.height()));
    back_button->setFlat(true);
    back_button->setStyleSheet("border:0px");

    red_apple_button->setIcon(QIcon(red_apple_image));
    red_apple_button->setIconSize(QSize(red_apple_image.width(),red_apple_image.height()));
    red_apple_button->setFlat(true);
    red_apple_button->setStyleSheet("border:0px");

    blue_bird->setFlat(true);
    blue_bird->setStyleSheet("border:0px");


    red_bird->setFlat(true);
    red_bird->setStyleSheet("border:0px");


    cool_bird->setFlat(true);
    cool_bird->setStyleSheet("border:0px");

    connect(m_Timer,&QTimer::timeout,[=](){
        updateInfo();
        update();
    });
    connect(back_button,&QPushButton::clicked,[=](){
        sendBackToStartScene();
        writeData_coin();
        writeBird();
        writeApple();
    });
    connect(red_apple_button,&QPushButton::clicked,[=](){
        buyApple();
        cout<<"****"<<endl;
    });
    connect(blue_bird,&QPushButton::clicked,this,&Shop::buyBlueBird);
    connect(red_bird,&QPushButton::clicked,this,&Shop::buyRedBird);
    connect(cool_bird,&QPushButton::clicked,this,&Shop::buyCoolBird);
}
void Shop::paintEvent(QPaintEvent *event){
    QPainter painter(this);

    painter.drawPixmap(0,0,shop_background.width(),shop_background.height(),shop_background);
    painter.drawPixmap(450-title_image.width()*0.5,30,title_image);
    painter.drawPixmap(450-red_apple_image.width()*0.5,red_apple_button->y(),red_apple_image);
    for(int i =0;i < 3;i++){
        painter.drawPixmap(250-copy.width()*0.5,400-copy.height()*0.5,blue_image[m_index]);
        painter.drawPixmap(450-copy.width()*0.5,400-copy.height()*0.5,red_image[m_index]);
        painter.drawPixmap(650-copy.width()*0.5,400-copy.height()*0.5,cool_image[m_index]);
    }

}
void Shop::updateInfo(){
    m_recoder++;

    if(m_recoder < BIRD_INTERVAL)
        return;

    m_recoder = 0;

    m_index++;

    if(m_index > 2)
        m_index = 0;
}
//返回界面的槽函数
void Shop::sendBackToStartScene(){
    emit backToSignal();
}
//读取金币信息
void Shop::readData_coin(){
    QFile file("coin_recoder.txt");
    if(!file.open(QIODevice::ReadOnly)){
        cout<<"failed"<<endl;
    }else{
        QDataStream stream(&file);
        stream>>money;
    }
}
void Shop::writeData_coin(){
    QFile file("coin_recoder.txt");
    if(!file.open(QIODevice::WriteOnly)){
        cout<<"failed"<<endl;
    }else{
        QDataStream stream(&file);

        stream<<money;
        file.close();
    }
}
//读取已购买的生命值的生命值信息
void Shop::readApple(){
    QFile file("apple_recoder.txt");
    if(!file.open(QIODevice::ReadOnly)){

    }else{
        QDataStream stream(&file);
        stream>>apples;
    }
}
void Shop::writeApple(){
    QFile file("apple_recoder.txt");
    if(!file.open(QIODevice::WriteOnly)){

    }else{
        QDataStream stream(&file);
        stream<<apples;
        file.close();
    }
}
void Shop::readBird(){
    QFile file("bird_recoder.txt");
    if(!file.open(QIODevice::ReadOnly)){

    }else{
        QDataStream stream(&file);
        stream>>bird_level;
    }
}
void Shop::writeBird(){
    QFile file("apple_recoder.txt");
    if(!file.open(QIODevice::WriteOnly)){

    }else{
        QDataStream stream(&file);
        stream<<bird_level;
        file.close();
    }
}
//买鸟有关金币的计算
void Shop::buyBlueBird(){

        if(money >= 50){

            money -= 50;
            bird_level = 2;
      }
}
void Shop::buyRedBird(){

        if(money >= 200){

            money -= 200;
            bird_level = 3;
        }



}
void Shop::buyCoolBird(){

        if(money >= 300){

            money -= 300;
            bird_level = 4;
        }
}
void Shop::buyApple(){

        if(money >= 50){

            money -= 50;
            apples++;
        }
}

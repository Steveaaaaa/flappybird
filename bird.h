﻿#ifndef BIRD_H
#define BIRD_H
#include<QPixmap>
#include<QVector>
class Bird
{
public:
    Bird();
    void setPosition(int x,int y);
    void updateInfo(int& min,int& max);
public:
    QPixmap m_Bird;
    QVector<QPixmap> m_birdArr;

    int m_X;
    int m_Y;
    int m_index;
    int m_recoder;
    int m_Life;
    QRect m_Rect;
};

#endif // BIRD_H

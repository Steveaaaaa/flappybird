﻿#include "tube.h"
#include<config.h>
#include<QTime>
#include<mainscene.h>
Tube::Tube()
{

    m_tube_up.load(TUBE_UP_PATH);
    m_tube_down.load(TUBE_DOWN_PATH);

    m_X = -100;
    m_Y = 0;

    m_Speed = TUBE_SPEED;

    m_Rect.setWidth(m_tube_up.width());
    m_Rect.setHeight(m_tube_up.height());
    m_Rect.moveTo(m_X,m_Y);
}
void Tube::tubePosition(){
    if(m_Free){
        return;
    }
    m_X -= m_Speed;
    m_Rect.moveTo(m_X,m_Y);
    if(m_X <= -60){
        m_Free = true;
    }
}

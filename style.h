﻿#ifndef STYLE_H
#define STYLE_H
#include<QLabel>
#include<QPixmap>
#include <QWidget>
#include<QPushButton>
#include<QVector>
#include<QString>
#include<QPainter>
#include<QTimer>
#include<QIcon>
#include<startscene.h>

class Style : public QWidget
{
    Q_OBJECT
public:
    explicit Style(QWidget *parent = nullptr);
    int m_index;
    int m_recoder;
    QPixmap copy;


    QPixmap style_background;

    QPixmap back_image;

    QPushButton *red_bird;
    QPushButton *blue_bird;
    QPushButton *cool_bird;

    QPushButton *back_button;

    QVector<QPixmap> red_image;
    QVector<QPixmap> blue_image;
    QVector<QPixmap> cool_image;
    QTimer *m_Timer;



    void paintEvent(QPaintEvent *event);
    void updateInfo();

signals:
    void backToSignal();
public slots:
    void sendBackToStartScene();
};

#endif // STYLE_H

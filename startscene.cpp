﻿#include "startscene.h"
#include<QPixmap>
#include<QPushButton>
#include<config.h>
#include<QIcon>
#include<QPainter>

startScene::startScene(QWidget *parent) : QWidget(parent)
{
    this->setFixedSize(GAME_WIDTH,GAME_HEIGHT);
    this->setWindowTitle(GAME_TITLE);
    this->setWindowIcon((QIcon)GAME_ICON);

   /* rank1 = new QLabel(this);
    rank2 = new QLabel(this);
    rank3 = new QLabel(this);
    rank4 = new QLabel(this);
    rank5 = new QLabel(this);*/
//按钮图片载入
    start_background.load(GAME_START_BACKGROUND);
    game_title.load(GAME_TITLE_PATH);
    game_title = game_title.scaled(game_title.width()*2,game_title.height()*2,Qt::KeepAspectRatio);
    style_image.load(MENU_BUTTON);
    start_image.load(START_BUTTON);
    shop_image.load(":/res/shop_button");
    style_image = style_image.scaled(style_image.width()*2,style_image.height()*2,Qt::KeepAspectRatio);
    start_image = start_image.scaled(start_image.width()*2,start_image.height()*2,Qt::KeepAspectRatio);
    shop_image = shop_image.scaled(shop_image.width()*2,shop_image.height()*2,Qt::KeepAspectRatio);
//开始按键载入
    start_button = new QPushButton("",this);
    start_button->resize(BUTTON_WIDTH*2,BUTTON_HEIGHT*2);
    start_button->setIcon(QIcon(start_image));
    start_button->setIconSize(QSize(BUTTON_WIDTH*2,BUTTON_HEIGHT*2));
    start_button->setFlat(true);
    start_button->setStyleSheet("border:0px");
    start_button->move(150,400);

    style_button = new QPushButton("",this);
    style_button->resize(BUTTON_WIDTH*2,BUTTON_HEIGHT*2);
    style_button->setIcon(QIcon(style_image));
    style_button->setIconSize(QSize(BUTTON_WIDTH*2,BUTTON_HEIGHT*2));
    style_button->setFlat(true);
    style_button->setStyleSheet("border:0px");
    style_button->move(375,400);

    shop_button = new QPushButton("",this);
    shop_button->resize(BUTTON_WIDTH*2,BUTTON_HEIGHT*2);
    shop_button->setIcon(QIcon(shop_image));
    shop_button->setIconSize(QSize(BUTTON_WIDTH*2,BUTTON_HEIGHT*2));
    shop_button->setFlat(true);
    shop_button->setStyleSheet("border:0px");
    shop_button->move(600,400);

    connect(start_button,&QPushButton::clicked,this,&startScene::sendStartSignal);
    connect(shop_button,&QPushButton::clicked,this,&startScene::sendShopSignal);
    connect(style_button,&QPushButton::clicked,this,&startScene::sendStyleSignal);
}

void startScene::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    painter.drawPixmap(0,0,start_background.width(),start_background.height(),start_background);
    painter.drawPixmap(100,100,game_title.width(),game_title.height(),game_title);
}
void startScene::sendStartSignal(){
    emit startSignal();
}
void startScene::sendShopSignal(){
    emit shopSignal();
}
void startScene::sendStyleSignal(){
    emit styleSignal();
}

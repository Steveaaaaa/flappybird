﻿#include "coin.h"
#include<config.h>
#include<mainscene.h>
Coin::Coin()
{
    coin_image.load(":/res/coin.png");
    coin_image = coin_image.scaled(coin_image.width()*0.5,coin_image.height()*0.5,Qt::KeepAspectRatio);

    m_X = -100;
    m_Y = 0;
    m_Rect.setWidth(coin_image.width());
    m_Rect.setHeight(coin_image.height());
    m_Rect.moveTo(m_X,m_Y);
    m_Speed = TUBE_SPEED;
}
//金币的轨迹计算
void Coin::coinPosition(){
    if(m_Free){
        return;
    }
    m_X -= m_Speed;
    m_Rect.moveTo(m_X,m_Y);
    if(m_X <= -60){
        m_Free = true;
    }
}

﻿#include "mainscene.h"
#include <QApplication>
#include<QPixmap>
#include<QSplashScreen>
#include<mainscene.h>
#include <QtGui/QPixmap>
#include <QtWidgets/QApplication>
#include<QTime>
#include<startscene.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//开始游戏的时候加载界面的显示
    QSplashScreen *splash = new QSplashScreen();

    splash->setPixmap(QPixmap(GAME_LOADING_PATH));
    splash->show();

    Qt::Alignment topRight = Qt::AlignRight | Qt::AlignTop;  //字体显示位置
    splash->showMessage(QObject::tr("Setting up the main window ..."), topRight, Qt::white);  //设置显示的文本内容，位置，字体颜色

    mainScene w;

    w.delayMSec(500);

    splash->showMessage(QObject::tr("Loading..."), topRight, Qt::white);
    w.delayMSec(1000);

    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    //w.initScene();

    splash->showMessage(QObject::tr("Will show software UI ..."), topRight, Qt::white);

    //w.delayMSec(1000);

    splash->finish(&w);
    delete splash;

    w.show();

    return a.exec();
}

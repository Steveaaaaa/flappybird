﻿#ifndef CONFIG_H
#define CONFIG_H
/***********游戏基本数据***********/
#define GAME_TITLE "FlappyBird v1.4)"
#define GAME_ICON ":/res/app.ico"
#define GAME_HEIGHT 504
#define GAME_WIDTH 900
#define GAME_RATE 10 //刷新间隔 单位毫秒
#define GAME_TITLE_PATH ":/res/game_title.png"
#define GAME_LOADING_PATH ":/res/loading_background.jpeg"
#define GAME_START_BACKGROUND ":/res/start_background.jpeg"
#define GAME_OVER_PATH ":/res/game_over.png"
#define BUTTON_WIDTH 80
#define BUTTON_HEIGHT 28
#define NUMBER_PATH ":/res/%1.png"
#define START_BUTTON ":/res/start_button.png"
#define MENU_BUTTON ":/res/menu_button.png"
#define MAP1_PATH ":/res/background.png"
#define BIRD_PATH ":/res/bird_%1.png"
#define BIRD_PATH1 ":/res/bird_1.png"
#define BIRD_NUM 3
#define BIRD_INTERVAL 20
#define INITIAL_SPEED 3.2
#define GRAVITY 0.09
#define MAP_SCROLL_SPEED 2
#define TUBE_SPEED MAP_SCROLL_SPEED
#define TUBE_UP_PATH ":/res/tube_up.png"
#define TUBE_DOWN_PATH ":/res/tube_down.png"
#define TUBE_NUM 20
#define SMALLEST_INTERVAL 100
#define TUBE_INTERVAL 60
#define COIN_NUM 20

#endif // CONFIG_H

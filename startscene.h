﻿#ifndef STARTSCENE_H
#define STARTSCENE_H
#include<QPushButton>
#include<QPixmap>
#include <QWidget>
#include<QLabel>
class startScene : public QWidget
{
    Q_OBJECT
public:
    explicit startScene(QWidget *parent = nullptr);
public:
    QPixmap game_title;
    QPixmap start_background;
    QPushButton *start_button;
    QPixmap start_image;
    QPushButton *style_button;
    QPixmap style_image;

    QPixmap shop_image;
    QPushButton *shop_button;
/*
    QLabel *rank1;
    QLabel *rank2;
    QLabel *rank3;
    QLabel *rank4;
    QLabel *rank5;*/




    void paintEvent(QPaintEvent *event);

signals:
    void startSignal();
    void shopSignal();
    void styleSignal();
public slots:
    void sendStartSignal();
    void sendShopSignal();
    void sendStyleSignal();
};

#endif // STARTSCENE_H

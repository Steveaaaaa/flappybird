﻿#ifndef SCOREBORAD_H
#define SCOREBORAD_H
#include<QPixmap>
#include<QString>
#include <QWidget>
#include<config.h>
#include<QVector>
class scoreBorad : public QWidget
{
    Q_OBJECT
public:
    explicit scoreBorad(QWidget *parent = nullptr);

    int score;
    int copy_Score;

    QVector<QPixmap> unit;
    QVector<QPixmap> ten;
    QVector<QPixmap> hundred;
    
    int m_Unit;
    int m_Ten;
    int m_Hundred;
    
    
    //void paintEvent(QPaintEvent *event);
    int getHundred(int score);
    int getTen(int score);
    int getUnit(int score);

signals:

public slots:
};

#endif // SCOREBORAD_H

﻿#ifndef SHOP_H
#define SHOP_H
#include<QLabel>
#include<QPixmap>
#include <QWidget>
#include<QPushButton>
#include<QVector>
#include<QString>
#include<QPainter>
#include<QTimer>
#include<QIcon>
#include<startscene.h>
#include<QDataStream>
#include<QFile>
#include<QDialog>
#include<QMessageBox>

class Shop : public QWidget
{
    Q_OBJECT
public:
    explicit Shop(QWidget *parent = nullptr);

    int m_index;
    int m_recoder;
    int money;
    int bird_level;
    int apples;
    QPixmap copy;
    QPixmap title_image;
    QLabel *shop_title;
    QPixmap shop_background;
    QPixmap red_apple_image;
    QPixmap back_image;

    QPushButton *red_bird;
    QPushButton *blue_bird;
    QPushButton *cool_bird;
    QPushButton *red_apple_button;
    QPushButton *back_button;

    QVector<QPixmap> red_image;
    QVector<QPixmap> blue_image;
    QVector<QPixmap> cool_image;
    QTimer *m_Timer;
    QLabel *label;

    void paintEvent(QPaintEvent *event);
    void updateInfo();
    void readData_coin();
    void writeData_coin();
    void readApple();
    void writeApple();
    void readBird();
    void writeBird();
signals:
    void backToSignal();
public slots:
    void sendBackToStartScene();
    void buyBlueBird();
    void buyRedBird();
    void buyCoolBird();
    void buyApple();
};

#endif // SHOP_H

﻿#include "style.h"
#include<config.h>
#include<QPainter>
#include<iostream>
Style::Style(QWidget *parent) : QWidget(parent)
{
    this->setFixedSize(GAME_WIDTH,GAME_HEIGHT);
    this->setWindowTitle(GAME_TITLE);
    this->setWindowIcon((QIcon)GAME_ICON);

    m_Timer = new QTimer;

    m_index = 0;
    m_recoder = 0;
    m_Timer->setInterval(GAME_RATE);


//小鸟样式选择界面的按钮初始化，设置等，以及选择效果的实现
    copy.load(":/res/bird_1.png");

    copy = copy.scaled(copy.width()*3,copy.height()*3,Qt::KeepAspectRatio);

    for(int i = 4 ;i <= 6 ;i++){
        //字符串拼接，类似  ":/res/bomb-1.png"
        QString str = QString(BIRD_PATH).arg(i);

        blue_image.push_back(QPixmap(str) = QPixmap(str).scaled(copy.width(),copy.height(),Qt::KeepAspectRatio));
    }
    for(int i = 7;i <= 9;i++){
        QString str1 = QString(BIRD_PATH).arg(i);
        red_image.push_back(QPixmap(str1) = QPixmap(str1).scaled(copy.width(),copy.height(),Qt::KeepAspectRatio));
    }
    for(int i = 10;i <= 12;i++){
        QString str2 = QString(BIRD_PATH).arg(i);
        cool_image.push_back(QPixmap(str2) = QPixmap(str2).scaled(copy.width(),copy.height(),Qt::KeepAspectRatio));

    }

    style_background.load(":/res/shop_background.jpeg");
    back_image.load(":/res/back_button.png");



    back_image = back_image.scaled(back_image.width()*2,back_image.height()*2,Qt::KeepAspectRatio);


    blue_bird = new QPushButton("",this);
    red_bird = new QPushButton("",this);
    cool_bird = new QPushButton("",this);
    back_button = new QPushButton("",this);


    blue_bird->resize(copy.width(),copy.height());
    red_bird->resize(copy.width(),copy.height());
    cool_bird->resize(copy.width(),copy.height());



    blue_bird->move(250-copy.width()*0.5,400-copy.height()*0.5);
    red_bird->move(400-copy.width()*0.5,400-copy.height()*0.5);
    cool_bird->move(650-copy.width()*0.5,400-copy.height()*0.5);
    back_button->move(GAME_WIDTH-back_image.width()-50,GAME_HEIGHT-back_image.height()-30);

    blue_bird->setVisible(false);
    red_bird->setVisible(false);
    cool_bird->setVisible(false);

    back_button->resize(back_image.width(),back_image.height());
    back_button->setIcon(QIcon(back_image));
    back_button->setIconSize(QSize(back_image.width(),back_image.height()));
    back_button->setFlat(true);
    back_button->setStyleSheet("border:0px");


    connect(m_Timer,&QTimer::timeout,[=](){
        updateInfo();
        update();
    });
    connect(back_button,&QPushButton::clicked,this,&Style::sendBackToStartScene);
}
void Style::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    painter.drawPixmap(0,0,style_background.width(),style_background.height(),style_background);
    for(int i =0;i < 3;i++){
        painter.drawPixmap(250-copy.width()*0.5,300-copy.height()*0.5,blue_image[m_index]);
        painter.drawPixmap(450-copy.width()*0.5,300-copy.height()*0.5,red_image[m_index]);
        painter.drawPixmap(650-copy.width()*0.5,300-copy.height()*0.5,cool_image[m_index]);
    }

}
void Style::updateInfo(){
    m_recoder++;

    if(m_recoder < BIRD_INTERVAL)
        return;

    m_recoder = 0;

    m_index++;

    if(m_index > 2)
        m_index = 0;
}
void Style::sendBackToStartScene(){
    emit backToSignal();
}

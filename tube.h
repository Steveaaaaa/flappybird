﻿#ifndef TUBE_H
#define TUBE_H
#include<QPixmap>
#include<QRect>

class Tube
{
public:
    Tube();

    void tubePosition();

public:
    QPixmap m_tube_up;
    QPixmap m_tube_down;

    int m_X;
    int m_Y;

    //bool movable;
    bool m_Free;
    bool m_Scoreed;

    QRect m_Rect;

    int m_Speed;
};

#endif // TUBE_H
